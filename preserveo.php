<?php
//https://codex.wordpress.org/Writing_a_Plugin
//https://codex.wordpress.org/Adding_Administration_Menus
//https://codex.wordpress.org/Plugin_API

/*
Plugin Name: Preserveo
Plugin URI: http://www.preserveo.com
Description: Preserveo content preservation integration.
Version: 1.0
*/

add_action( 'admin_menu', 'preserveo_menu' );

function preserveo_menu() {
  add_options_page( 'Preserveo', 'Preserveo', 'manage_options', 'preserveo-admin', 'preserveo_admin_form' );
}

function preserveo_admin_form() {
  if (!current_user_can('manage_options'))
  {
    wp_die( __('You do not have sufficient permissions to access this page.') );
  }
  echo "hello";
}